/**
 *
 * @file /src/bpresolution.cpp
 * @author Séraphin Costa
 * @version 1.0
 * @date 2020/11/29
 *
 */

#include "bpresolution.h"

#include <algorithm>
#include <cmath>
#include <iterator>
#include <utility>
#include <vector>

#include "bpinstance.h"

BPResolution::BPResolution():
	m_uNbInstances(0),
	m_tblInstances(nullptr)
{ }

BPResolution::BPResolution(const BPResolution& other):
	m_uNbInstances(other.m_uNbInstances),
	m_tblInstances(new BPInstance[m_uNbInstances])
{
	for(std::size_t i=0; i<m_uNbInstances; ++i)
		m_tblInstances[i] = other.m_tblInstances[i];
}

BPResolution::BPResolution(std::size_t uNbInstances, BPInstance* tblInstances):
	m_uNbInstances(uNbInstances),
	m_tblInstances(tblInstances)
{ }

BPResolution::~BPResolution()
{
	if(m_tblInstances != nullptr)
		delete[] m_tblInstances;
}

BPResolution& BPResolution::operator=(const BPResolution& other)
{
	if(m_tblInstances != nullptr)
		delete[] m_tblInstances;

	m_uNbInstances = other.m_uNbInstances;
	m_tblInstances = new BPInstance[m_uNbInstances];

	for(std::size_t i=0; i<m_uNbInstances; ++i)
		m_tblInstances[i] = other.m_tblInstances[i];

	return *this;
}

void BPResolution::setInstance(const BPInstance& instance)
{
	if(m_tblInstances != nullptr)
		delete[] m_tblInstances;

	m_uNbInstances = 1;
	m_tblInstances = new BPInstance[1];
	m_tblInstances[0] = std::move(instance);
}

void BPResolution::setInstances(std::size_t uNbInstances, BPInstance* tblInstances)
{
	if(m_tblInstances != nullptr)
		delete[] m_tblInstances;

	m_uNbInstances = uNbInstances;
	m_tblInstances = tblInstances;
}

std::size_t BPResolution::borneInferieure(std::size_t index)
{
	BPInstance& inst = m_tblInstances[index];

	double sumPoids = 0.;
	for(std::size_t i=0; i<inst.nbObjets(); ++i)
		sumPoids += inst.poids(i);

	return static_cast<std::size_t>(std::ceil(sumPoids));
}

std::size_t BPResolution::nbInstances() const
{
	return m_uNbInstances;
}

const BPInstance& BPResolution::instance(std::size_t index) const
{
	return m_tblInstances[index];
}

std::size_t BPResolution::nextFit(std::size_t index)
{
	BPInstance& inst = m_tblInstances[index];

	std::vector<double> tblSacs(inst.nbObjets(), 0.); // Volume d'objets dans le sac (somme des t_j pour les o_j dans S_i)

	std::size_t numSac = 0;
	for(std::size_t i=0; i<inst.nbObjets(); ++i)
	{
		// si le sac est trop plein pour o_i on passe au suivant
		if(tblSacs[numSac]+inst.poids(i) > 1.)
			++numSac;

		tblSacs[numSac] += inst.poids(i);
	}

	return numSac+1;
}

std::size_t BPResolution::firstFitDecreasing(std::size_t index)
{
	BPInstance& inst = m_tblInstances[index];

	// On copie le tableau des poids des objets
	std::vector<double> tblPoids;
	tblPoids.reserve(inst.nbObjets());
	for(std::size_t i=0; i<inst.nbObjets(); ++i)
		tblPoids.push_back(inst.poids(i));

	// On le tri par ordre décroissant
	std::sort(tblPoids.rbegin(), tblPoids.rend());

	std::vector<double> tblSacs(inst.nbObjets(), 0.); // Volume d'objets dans le sac (somme des t_j pour les o_j dans S_i)

	std::size_t numSac = 0;

	for(double poids : tblPoids) // On prend les objet dans l'ordre décroissant
	{
		// Recherche du premier sac qui peut contenir o_i
		std::size_t selectSac = 0;
		while(selectSac < numSac && tblSacs.at(selectSac)+poids > 1.)
			++selectSac;

		// S'il n'y en a pas on prend un nouveau sac
		if(selectSac >= numSac)
			++numSac;

		tblSacs[selectSac] += poids;
	}

	return numSac;
}

std::size_t BPResolution::randOrderRandBin(std::size_t index)
{
	BPInstance& inst = m_tblInstances[index];

	// On copie le tableau des poids des objets
	std::vector<double> tblPoids;
	tblPoids.reserve(inst.nbObjets());
	for(std::size_t i=0; i<inst.nbObjets(); ++i)
		tblPoids.push_back(inst.poids(i));

	// Mélange aléatoire des objets
	std::random_shuffle(tblPoids.rbegin(), tblPoids.rend());

	std::vector<double> tblSacs(inst.nbObjets(), 0.); // Volume d'objets dans le sac (somme des t_j pour les o_j dans S_i)

	std::size_t numSac = 0; // nombre de sacs non vide

	for(double poids : tblPoids)
	{
		std::size_t nbSacDispo = std::count_if(tblSacs.begin(), std::next(tblSacs.begin(), numSac),
										  [poids](double val){ return (val+poids <= 1.); });

		if(nbSacDispo <= 0) // pas de sac disponible -> nouveau sac
		{
			tblSacs[numSac] += poids;
			++numSac;
		}
		else // sinon, on en prend un aléatoirement
		{
			std::size_t selectSac = std::rand()%nbSacDispo; // parmi les sacs dispos

			// on recalcule l'indice du sac dans le tableau avec tout les sacs
			std::size_t i = 0;
			std::vector<double>::iterator it = tblSacs.begin();
			while(i <= selectSac)
			{
				double val = *it;
				if(val+poids > 1.) // sac dispo ?
					++selectSac;

				++it;
				++i;
			}

			tblSacs[selectSac] += poids;
		}
	}

	return numSac;
}
