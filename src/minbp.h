#ifndef MINBP_H_INCLUDED
#define MINBP_H_INCLUDED
/**
 *
 * @file /src/minbp.h
 * @author Séraphin Costa
 * @version 1.0
 * @date 2020/12/01
 *
 */

#include <initializer_list>
#include <ostream>

#include "bpresolution.h"

enum INSTANCE_MODE
{
	IM_FICHIER = 0, // I_f
	IM_CLAVIER = 1, // I_c
	IM_ALEAMULTI = 2 // I_am
};

struct ResultatMinBPAlgo {
	std::size_t result;
	double ratio;
};

struct ResultatMinBP {
	std::size_t bornInferieure;
	ResultatMinBPAlgo NF;
	ResultatMinBPAlgo FFD;
	ResultatMinBPAlgo RORB;
};

class MinBP
{
public:
	MinBP();

	void start();
	void loadInstanceFromFile();
	void loadInstanceFromCin();
	void createRandomInstances();

	void startInstance(std::size_t index, ResultatMinBP& result);

	int askQuestion(const char* question, const std::initializer_list<const char*>& propositions);

private:
	BPResolution m_minBPAlgo;
};

// Fonction d'affichage des résultats d'une instance
std::ostream& operator<<(std::ostream& stream, const ResultatMinBP& result);

#endif // MINBP_H_INCLUDED
