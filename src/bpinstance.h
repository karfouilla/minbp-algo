#ifndef BPINSTANCE_H_INCLUDED
#define BPINSTANCE_H_INCLUDED
/**
 *
 * @file /src/bpinstance.h
 * @author Séraphin Costa
 * @version 1.0
 * @date 2020/11/29
 *
 */

#include <cstddef>
#include <istream>
#include <ostream>

class BPInstance
{
public:
	static BPInstance fromStream(std::istream& file); // Charge une instance depuis un fichier/clavier
	static BPInstance fromRandom(std::size_t uNbObjets); // Génération d'une instance aléatoire

public:
	BPInstance(); // Constructeur par défaut
	BPInstance(const BPInstance& other); // Constructeur de copie
	BPInstance(BPInstance&& other); // Constructeur par déplacement (rend other inutilisable)
	BPInstance(std::size_t uNbObjets, double* tblPoids);
	~BPInstance();

	BPInstance& operator=(const BPInstance& other); // Affectation
	BPInstance& operator=(BPInstance&& other); // Affectation par déplacement (rend other inutilisable)

	void setData(std::size_t uNbObjets, double* tblPoids);

	std::size_t nbObjets() const;

	double* poids() const;
	double poids(std::size_t index) const;

private:
	std::size_t m_uNbObjets;
	double* m_tblPoids;
};

// Fonction d'affichage d'une instance
std::ostream& operator<<(std::ostream& stream, const BPInstance& instance);

#endif // BPINSTANCE_H_INCLUDED
