/**
 *
 * @file /src/bpinstance.cpp
 * @author Séraphin Costa
 * @version 1.0
 * @date 2020/11/29
 *
 */

#include "bpinstance.h"

BPInstance BPInstance::fromStream(std::istream& file)
{
	std::size_t uNbObjets = 0;
	double* tblPoids = nullptr;
	file >> uNbObjets;
	file.ignore(); // ignore le caractère ':'

	if(uNbObjets > 0)
		tblPoids = new double[uNbObjets];

	for(std::size_t i=0; i<uNbObjets; ++i)
	{
		file >> tblPoids[i];
		file.ignore(); // ignore le caractère ':'
	}

	return BPInstance(uNbObjets, tblPoids);
}

BPInstance BPInstance::fromRandom(std::size_t uNbObjets)
{
	double* tblPoids = new double[uNbObjets];

	for(std::size_t i=0; i<uNbObjets; ++i)
		tblPoids[i] = static_cast<double>(std::rand())/static_cast<double>(RAND_MAX);

	return BPInstance(uNbObjets, tblPoids);
}

BPInstance::BPInstance():
	m_uNbObjets(0),
	m_tblPoids(nullptr)
{ }

BPInstance::BPInstance(const BPInstance& other):
	m_uNbObjets(other.nbObjets()),
	m_tblPoids(new double[m_uNbObjets])
{
	for(std::size_t i=0; i<m_uNbObjets; ++i)
		m_tblPoids[i] = other.poids(i);
}

BPInstance::BPInstance(BPInstance&& other):
	m_uNbObjets(other.m_uNbObjets),
	m_tblPoids(other.m_tblPoids)
{ }

BPInstance::BPInstance(std::size_t uNbObjets, double* tblPoids):
	m_uNbObjets(uNbObjets),
	m_tblPoids(tblPoids)
{ }

BPInstance::~BPInstance()
{
	delete[] m_tblPoids;
}

BPInstance& BPInstance::operator=(const BPInstance& other)
{
	if(m_tblPoids != nullptr)
		delete[] m_tblPoids;

	m_uNbObjets = other.nbObjets();
	m_tblPoids = new double[m_uNbObjets];
	for(std::size_t i=0; i<m_uNbObjets; ++i)
		m_tblPoids[i] = other.poids(i);

	return *this;
}

BPInstance& BPInstance::operator=(BPInstance&& other)
{
	if(m_tblPoids != nullptr)
		delete[] m_tblPoids;

	m_uNbObjets = other.m_uNbObjets;
	m_tblPoids = other.m_tblPoids;

	other.m_uNbObjets = 0;
	other.m_tblPoids = nullptr;
	return *this;
}

void BPInstance::setData(std::size_t uNbObjets, double* tblPoids)
{
	if(m_tblPoids != nullptr)
		delete[] m_tblPoids;

	m_uNbObjets = uNbObjets;
	m_tblPoids = tblPoids;
}

std::size_t BPInstance::nbObjets() const
{
	return m_uNbObjets;
}

double* BPInstance::poids() const
{
	return m_tblPoids;
}

double BPInstance::poids(std::size_t index) const
{
	return m_tblPoids[index];
}


std::ostream& operator<<(std::ostream& stream, const BPInstance& instance)
{
	stream << instance.nbObjets();
	for(std::size_t i=0; i<instance.nbObjets(); ++i)
		stream << ":" << instance.poids(i);
	return stream;
}
