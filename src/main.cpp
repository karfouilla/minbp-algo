/**
 *
 * @file /src/main.cpp
 * @author Séraphin Costa
 * @version 1.0
 * @date 2020/11/29
 *
 */

#include <ctime>
#include <iostream>
#include <fstream>

#include "bpinstance.h"
#include "bpresolution.h"
#include "minbp.h"

int main(int , char** )
{
	// Initialisation de l'aléatoire
	std::srand(static_cast<unsigned>(std::time(nullptr)));

	// 9:.4:.1:.6:.3:.5:.3:.3:.3:.1
	std::cerr << "minbp-algo est un programme développer dans le cadre du projet Bin-Packing" << std::endl;
	std::cerr << "pour le module X1II030 Complexité et algorithmes à l'université de Nantes" << std::endl;
	std::cerr << "il a pour objectif de tester le problème Min Bin-Packing définit comme suit :" << std::endl << std::endl;

	std::cerr << "-------------------------- MINIMUMBIN-PACKING(MIN-BP) --------------------------" << std::endl
			  << "-- Instance : Un ensemble d’objets O = {o_1, o_2 ... o_n} où chaque objet o_i --" << std::endl
			  << "-- possède une taille 0 < t_i ≤ 1, un ensemble de sacs S = {S_1, S_2... S_n}. --" << std::endl
			  << "-- Solution : Une répartition des objets de O dans les sacs de S telle que    --" << std::endl
			  << "-- pour tout 1 ≤ j ≤ n, la somme des tailles des objets de S_j est inférieure --" << std::endl
			  << "-- ou ́egale à 1.                                                              --" << std::endl
			  << "-- Mesure : m, le nombre de sacs non vides.                                   --" << std::endl
			  << "--------------------------------------------------------------------------------" << std::endl << std::endl;

	std::cerr << "Les instance de ce problème doivent être entrée sous la forme suivante :" << std::endl;
	std::cerr << "n:t_1:t_2:t_3 ... t_n" << std::endl;
	std::cerr << "Avec n le nombre d'objets et de sacs, t_1, t_2, t_3 ... t_n le poids des objets." << std::endl << std::endl;

	MinBP program;
	program.start();
	return 0;
}
