/**
 *
 * @file /src/minbp.cpp
 * @author Séraphin Costa
 * @version 1.0
 * @date 2020/12/01
 *
 */

#include "bpinstance.h"
#include "minbp.h"

#include <iostream>
#include <fstream>

MinBP::MinBP():
	m_minBPAlgo()
{ }

void MinBP::start()
{
	int mode = askQuestion("Choisissez le mode de saisie des instances d'entrée :",
						   {"Depuis un fichier", // I_f
							"Au clavier", // I_c
							"Génération aléatoire de plusieurs instances"}); // I_am

	if(mode == IM_FICHIER) // I_f
	{
		loadInstanceFromFile();
	}
	else if(mode == IM_CLAVIER) // I_c
	{
		loadInstanceFromCin();
	}
	else if(mode == IM_ALEAMULTI) // I_am
	{
		createRandomInstances();
	}

	if(mode == IM_FICHIER || mode == IM_CLAVIER)
	{
		ResultatMinBP result;
		startInstance(0, result);
		std::cout << result << std::endl;
	}
	else if(mode == IM_ALEAMULTI)
	{
		double ratioMoyenNF = 0.;
		double ratioMoyenFFD = 0.;
		double ratioMoyenRORB = 0.;
		for(std::size_t i=0; i<m_minBPAlgo.nbInstances(); ++i)
		{
			ResultatMinBP result;
			startInstance(i, result);
			ratioMoyenNF += result.NF.ratio;
			ratioMoyenFFD += result.FFD.ratio;
			ratioMoyenRORB += result.RORB.ratio;
		}
		ratioMoyenNF /= static_cast<double>(m_minBPAlgo.nbInstances());
		ratioMoyenFFD /= static_cast<double>(m_minBPAlgo.nbInstances());
		ratioMoyenRORB /= static_cast<double>(m_minBPAlgo.nbInstances());

		std::cout << "Ratio moyen NF = " << ratioMoyenNF << std::endl;
		std::cout << "Ratio moyen FFD = " << ratioMoyenFFD << std::endl;
		std::cout << "Ratio moyen RORB = " << ratioMoyenRORB << std::endl;
	}
}

void MinBP::loadInstanceFromFile()
{
	std::string filename;

	std::cout << "Nom du fichier (sans espace) : ";
	std::cin >> filename;

	std::ifstream file(filename);
	m_minBPAlgo.setInstance(BPInstance::fromStream(file));
	file.close();
}

void MinBP::loadInstanceFromCin()
{
	std::cout << "Instance : ";
	m_minBPAlgo.setInstance(BPInstance::fromStream(std::cin));
}

void MinBP::createRandomInstances()
{
	std::size_t nbInstance; // p
	std::cout << "Nombre d'instance à générer : ";
	std::cin >> nbInstance;

	std::size_t nbObjets; // n
	std::cout << "Nombre d'objet par instances : ";
	std::cin >> nbObjets;

	BPInstance* instances = new BPInstance[nbInstance];

	for(std::size_t i=0; i<nbInstance; ++i)
		instances[i] = BPInstance::fromRandom(nbObjets);

	m_minBPAlgo.setInstances(nbInstance, instances);
}

void MinBP::startInstance(std::size_t index, ResultatMinBP& result)
{
	result.bornInferieure = m_minBPAlgo.borneInferieure(index);

	result.NF.result = m_minBPAlgo.nextFit(index);
	result.FFD.result = m_minBPAlgo.firstFitDecreasing(index);
	result.RORB.result = m_minBPAlgo.randOrderRandBin(index);

	result.NF.ratio = static_cast<double>(result.NF.result)/static_cast<double>(result.bornInferieure);
	result.FFD.ratio = static_cast<double>(result.FFD.result)/static_cast<double>(result.bornInferieure);
	result.RORB.ratio = static_cast<double>(result.RORB.result)/static_cast<double>(result.bornInferieure);
}

int MinBP::askQuestion(const char* question, const std::initializer_list<const char*>& propositions)
{
	int reponse = 0;
	std::cout << question << std::endl;
	int i = 1;
	for(const char* proposition : propositions)
	{
		std::cout << i << ". " << proposition << std::endl;
		++i;
	}
	std::cout << "Réponse (1.." << (i-1) << ") :";
	std::cin >> reponse;
	return (reponse-1);
}


std::ostream& operator<<(std::ostream& stream, const ResultatMinBP& result)
{
	stream << "Borne inférieure = " << result.bornInferieure << std::endl;
	stream << std::endl;

	stream << "Résultat NF = " << result.NF.result << std::endl;
	stream << "ratio NF = " << result.NF.ratio << std::endl << std::endl;

	stream << "Résultat FFD = " << result.FFD.result << std::endl;
	stream << "ratio FFD = " << result.FFD.ratio << std::endl << std::endl;

	stream << "Résultat RORB = " << result.RORB.result << std::endl;
	stream << "ratio RORB = " << result.RORB.ratio;
	return stream;
}
