#ifndef BPRESOLUTION_H_INCLUDED
#define BPRESOLUTION_H_INCLUDED
/**
 *
 * @file /src/bpresolution.h
 * @author Séraphin Costa
 * @version 1.0
 * @date 2020/11/29
 *
 */

#include <cstddef>

class BPInstance;

class BPResolution
{
public:
	BPResolution(); // Constructeur par défaut
	BPResolution(const BPResolution& other); // Constructeur de copie
	BPResolution(std::size_t uNbInstances, BPInstance* tblInstances); // Constructeur avec les paramètres
	~BPResolution();

	BPResolution& operator=(const BPResolution& other); // Affectation

	void setInstance(const BPInstance& instance);
	void setInstances(std::size_t uNbInstances, BPInstance* tblInstances);

	std::size_t nbInstances() const;
	const BPInstance& instance(std::size_t index) const;

	/**
	 * Effectue l'algorithme Next Fit sur l'instance numéro index (0 par défaut)
	 */
	std::size_t nextFit(std::size_t index = 0);
	/**
	 * Effectue l'algorithme First Fit Decreasing sur l'instance numéro index (0 par défaut)
	 */
	std::size_t firstFitDecreasing(std::size_t index = 0);
	/**
	 * Effectue l'algorithme Random Order/Random Bin sur l'instance numéro index (0 par défaut)
	 */
	std::size_t randOrderRandBin(std::size_t index = 0);
	/**
	 * Retourne la borne inférieure pour l'instance numéro index (0 par défaut)
	 */
	std::size_t borneInferieure(std::size_t index = 0);


private:
	std::size_t m_uNbInstances;
	BPInstance* m_tblInstances;
};

#endif // BPRESOLUTION_H_INCLUDED
